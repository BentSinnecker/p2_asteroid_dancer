﻿using System;
using UnityEngine;

namespace PTWO_PR
{
    public class PlayerLauncher : MonoBehaviour
    {
        [Header("Base Variables")]
        [SerializeField]    private int launchSpeed;
        
        [SerializeField]    private Rigidbody2D playerRigidbody;
        
                            private bool hasPlayerLaunched;
        /*
         * These variables provide everything we need for pushing an object in one direction. We've got a speed value
         * which can be defined in the Unity editor, a reference to the "Rigidbody2D" component of the object this
         * script is attached to and a bool to check whether the object has launched or not.
         */

        private void Awake()
        {
            hasPlayerLaunched = false;
            /*
             * At the start of the scene, we set the "hasPlayerLaunched" check to false, to enable the mechanic in the
             * "Update" method
             */
        }

        private void Update()
        {
            /*
             * Because we're applying an impulse force to the object here, we are using the simple "Update" method and
             * not "FixedUpdate"
             */ 
            
            
            if (Input.GetKeyDown(KeyCode.Space) && (hasPlayerLaunched != true))
            {
                // Checks if the key "Space" is pressed on the keyboard and the "hasPlayerLaunched" bool is set to "false"
                
                
                playerRigidbody.AddForce(new Vector2(0, launchSpeed));
                hasPlayerLaunched = true;
                /*
                 * If so, we're adding a force in form of a Vector2 to the object and set the "launchSpeed" variable
                 * to define the speed at which the object is pushed on the "Y" axis. After that we set the
                 * "hasPlayerLaunched" bool to true, to prevent the object from being pushed multiple times.
                 */
            }
        }
    }
}