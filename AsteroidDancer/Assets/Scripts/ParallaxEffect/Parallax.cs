using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PTWO_PR
{
    public class Parallax : MonoBehaviour
    {
        [Header("References:")]
        [SerializeField] private Camera cam;
        [SerializeField] private Transform player;
        /*
         * We need both of these variables to refernece the Main Camera and the Player object in the Unity Inspector
         * for the effect to work.
         */

        Vector2 startPosition;
        float startZ;
        /* 
         * Later in the script we need an initial start position of the player. Therefore we store the X- and Y-Position seperatly from 
         * the Z-Axis, since we need the spereate Z-Position later on. 
         */

        /*
         * For the parallax effect to work we need to set up some properties which are being updated every frame 
         * and calculated every time we call them . 
         */

        Vector2 travel => (Vector2)cam.transform.position - startPosition; 
        // This property is defines the distance the camera has moved from the original position of the player sprite.
        float distanceFromSubject => transform.position.z - player.position.z;
        // Through this property we know the distance from the background layer to the main camera which creates the effect
        // of moving slow or fast.
        float clippingPlane => (cam.transform.position.z + (distanceFromSubject > 0 ? cam.farClipPlane : cam.nearClipPlane));
        // Here we state that if the layer is BEHIND the player sprite, the layers uses the far clipping plane which moves with the player
        // If the layer is INFRONT of the player it uses the near clipping plane which means that it'll move in the opposite direction.
        float parallaxFactor => Mathf.Abs(distanceFromSubject) / clippingPlane;
        // This defines the Parallax Factor (the further away from the camera the slower the layer moves)

        private void Start()
        {
            startPosition = transform.position;
            startZ = transform.position.z;
            /*
             * In this method, we save the position of the object (the Player). 
             * Since we stored the x- and y- value seperate from the z- value, we need to address it here one more time. 
             */
        }

        private void Update()
        {
            Vector2 newPos = startPosition + travel * parallaxFactor;
            transform.position = new Vector3(newPos.x, newPos.y, startZ);
            /*
             * This Method moves the background in the same speed as the camera.
             * But since we defined the parallax Factor (which is lesser than 0), 
             * the background gets slowly left behind by the camera creating a Parallax Effect.
             * Furthermore we dont want to reset the z-Position every frame so we store a new Vector3 as a new position.
             */
        }
    }
}
