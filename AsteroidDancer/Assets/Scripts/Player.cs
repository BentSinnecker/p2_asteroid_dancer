using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PTWO_PR
{
    public class Player : MonoBehaviour
    {
        
        [Header("Base Variables")]
        // The base variables essentially contain variables for moving the player
        
        [SerializeField]    private float thrustSpeed;

        [SerializeField]    private float jukeSpeed;
        
                            private bool isPlayerThrusting;

                            private float turnDirection;

                            private Vector3 direction;
        /*
         * Vector3 is used to access the transform component of the player object in Unity.
         * It allows us to manipulate the current position of the player object and
         * to set a direction to where the object is supposed to go.
         */
        

                            private Rigidbody2D _rigidbody2D;
        /*
         * This variable allows us access to the Rigidbody2D component that has been attached
         * to the player object in Unity. Using this variable allows us to manipulate the physics
         * that this component enables on that specific object.
         */

        [SerializeField]    private AudioSource dashSound;
        //This variable addresses the audio source which is required to play the dash.

        [Header("Fuel Limit")]
        /*
         * These variables are used for the fuel limit mechanic which prevents the player
         * from overusing the keyboard inputs.
         */
        [SerializeField] private float maxFuel;

        [SerializeField] private float currentFuel;

        [SerializeField] private int rechargeSize;

        [SerializeField] private float fuelCostForward;
        
        [SerializeField] private float fuelCostDash;

       
        [Header("Fuel Items")]
        [SerializeField] private float fuelItemValue;
        // This variable is used to define the value of the collectable fuel items (how much fuel they replenish)
        

        [Header("Fuel Bar (UI)")]
        [SerializeField] private FuelBar fuelBar;
        // This variable allows us to access and manipulate the contains of the FuelBar script
      
        
        private void Awake() // This method gets called before all other methods
        {
            _rigidbody2D = GetComponent<Rigidbody2D>();
            /*
             * We're setting our _rigidbody2D variable equal to  GetComponent, which is looking for the Rigidbody2D
             * component that is attached to the object this script is attached to. This allows us to fully utilize
             * the Rigidbody2D component
             */

            
            currentFuel = maxFuel;
            //Sets the fuel counter to max at the beginning of each play-session
            
            fuelBar.SetMaxFuel(maxFuel);
            /*
             * Here we access the script "FuelBar" by writing the name of the previously defined variable "fuelBar".
             * Using the point operator ".", we can access the "SetMaxFuel" method, which we defined in the FuelBar
             * script as well. The SetMaxFuel method essentially sets the slider, that indicates how
             * much fuel has been used while playing, to maximum.
             */
        }


        private void Update()
        {
            PlayerJuke();
            /*
             * Here we call the "PlayerJuke" method which is defined below. The reason why it is called here and not in
             * the "FixedUpdate" method: Any "[...]ButtonDown" or "[...]ButtonUp" method doesn't work reliably in the
             * "FixedUpdate" method. Since "FixedUpdate" doesn't necessarily run every single frame, the key inputs can be
             * missed sometimes which results in an unstable playing experience (key inputs sometimes simply won't work)
             */
        }

        private void FixedUpdate() //Update gets called on a fixed time interval 
        {
            /*
             * For this physics-based part of the code, we're using the "FixedUpdate" method rather than the standard "Update".
             * The reason for that is that the action below is applying a continuous force to the object,
             * which works more reliably in the "FixedObject" method. Impulse forces however, as defined the "PlayerJuke" method,
             * work better in the standard "Update" method.
             */
            
            
            isPlayerThrusting = Input.GetKey(KeyCode.W);
            /*
             * Sets the bool equal to the input so we only have to use the bool in the following if function.
             * The input checks if the "W" button is pressed and held
             */
            
            if (isPlayerThrusting && currentFuel > fuelCostForward)
            {
                _rigidbody2D.AddForce(transform.up * thrustSpeed);
                currentFuel -= fuelCostForward;
                
                fuelBar.SetFuel(currentFuel);
            }
            /*
             * This if-function checks if the "isPlayerThrusting" bool variable is "true" (and also if the key "W" is
             * being pressed, as explained above). Additionally, the function checks if the current fuel variable is bigger
             * than the determined cost for thrusting forward. This helps putting a limit to the amount forward gas the player
             * has. If one or both of these if-queries turn out to be false, the player won't be able to move the player
             * object forward.
             * 
             * To move the object, we are using the "AddForce" method and call it in the player-objects rigidbody
             * component. By using the shortcut "transform.up" we're telling the object that it can move up on the "Y" axis. 
             * This multiplied by the variable "thrustSpeed" (which values are also defined in the Unity inspector of
             * the game-object the script is attached to and we have our speed for thrusting forward.
             * 
             * After that we tell the script to subtract the amount of fuel cost from the current fuel amount
             * for thrusting forward (we've also determined that amount in the objects Unity inspector). This means
             * that the fuel bar now steadily empties as long as the button for thrusting forward is pressed.
             * After that we set the current fuel bar value to the value of the "currentFuel" variable.
             *
             * Steadily moving the player gets called here because of the dependency on the physics system.
             * If these kind of inputs would be called in "Update", the physics would always be depending on
             * the frame rate the game is currently running on, which is going to lead to inconsistent physics.
             * "FixedUpdate" makes sure that the physics are consistent because of the fixed time interval it runs on.
             * Inputs that rely on a single button press however, get called in the simple "Update" method, as explained
             * above.
             */


            FuelRecharge();
            // Method used to refill the fuel meter
        }
        

        private void PlayerJuke()
        {
            float playerSpeed = _rigidbody2D.velocity.magnitude;
            /*
             * This local float variable is used to shorten a check that is used to determine the speed of the
             * objects rigidbody this script is attached to. We are needing this information for the juke commands
             * written below.
             */

            if (Input.GetKeyDown(KeyCode.A) && (currentFuel > fuelCostDash))
            {
                /*
                 * This if-function only works if the key "A" is pressed once AND the current fuel meter isn't smaller than
                 * the value of the variable "fuelCostDash", which can be manipulated in the Unity inspector of the player
                 * object.
                 */
                
                _rigidbody2D.AddForce(new Vector2(-jukeSpeed,playerSpeed));
                
                currentFuel -= fuelCostDash;
                
                fuelBar.SetFuel(currentFuel);
                /*
                 * If the conditions named above are true, add force to the rigidbody we're attached to. In the Vector2
                 * constructor, we determine the direction the force should be applied, which is the negative value of
                 * the variable "jukeSpeed" (value also determined in the Unity inspector) on the "X" axis and the current
                 * speed of the object, which we stored in the local variable at the beginning of this method.
                 * Inserting this flexible value on the "Y" axis instead of a fixed value like 0 or 1 helps because
                 * otherwise the object would be stopped, slowed down or sped up, depending on the current speed of
                 * the object (the object is constantly moving upwards on the "Y" axis).
                 *
                 * Also the value of the "currentFuel" variable will be reduced by the value of the "fuelCostDash"
                 * variable, which can also be manipulated in the Unity inspector.
                 *
                 * At the end: Set the fuel bar UI element equal to the "currentFuel" variable, so the loss of fuel
                 * can be displayed.
                 * 
                 */

                dashSound.Play();
                //When the key is pressed the audio attached to the source will be played
            }
            else if (Input.GetKeyDown(KeyCode.D)  && (currentFuel > fuelCostDash))
            {
                _rigidbody2D.AddForce(new Vector2(jukeSpeed,playerSpeed));
                
                currentFuel -= fuelCostDash;
                
                fuelBar.SetFuel(currentFuel);
                /*
                 * Essentially does the same as the if function above, but with a positive "jukeSpeed" value.
                 */

                dashSound.Play();
                //When the key is pressed the audio attached to the source will be played
            }
        }

        private void FuelRecharge()
        {
            // This method is used to recharge the "currentFuel" variable. This method is called in the "Update" method

            
            if (currentFuel < maxFuel)
            {
                // Checks if the value of "currentFuel is smaller than "maxFuel"
                
                currentFuel += rechargeSize * Time.deltaTime;
                /*
                 * If so, add the value of the variable "rechargeSize" times the time that has passed since the last
                 * frame
                 */

                fuelBar.SetFuel(currentFuel);
                //Set the "fuelBar" back to the "currentFuel" value that just has been determined above
                
                if (currentFuel <= 0)
                {
                    currentFuel = 0;
                }
                /*
                 * This if-function prevents the "currentFuel" value to get below 0. This way it's guaranteed that no
                 * action that consumes fuel can get this variable below that value and lead to the fuel bar not working
                 * properly
                 */
            }
            else
            {
                currentFuel = maxFuel;
            }
            /*
             * If the "currentFuel" value isn't smaller than the value of "maxFuel", simply set the "currentFuel" value
             * equal to "maxFuel".
             */
            
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            // Through this method the player is able to collect Fuel items to replenish his/ her fuel tank 
             
            
            if (collision.CompareTag(GameTags.COLLECTIBLE))
            {
                currentFuel += fuelItemValue;
                Destroy(collision.gameObject);
                /*
                 * The fuelItemValue can be altered through the unity inspector which restocks the fuel. After being picked up
                 * the Game Object will be destroyed
                 */
            }

            if (collision.CompareTag(GameTags.DEATHZONE))
            {
                SceneManager.LoadScene("MAIN_GAMEOVER");
                /*
                 * Here we send the player to the "Game Over" screen, once this game object collides with the deathzone.
                 * For that we use the "CompareTag" method and then access the scene manager. 
                 */

            }
        }
    }
}


