﻿using UnityEngine;
using UnityEngine.UIElements;

namespace PTWO_PR.GrapplingHook3
{
    public class GrapplingHookV3 : MonoBehaviour
    {
        // This script lets us use a grappling hook in the game
        
        
        [Header("Base Variables")]
        [SerializeField]    private Camera mainCamera;
        
        [SerializeField]    private LineRenderer _lineRenderer;
        
        [SerializeField]    private DistanceJoint2D _distanceJoint;
        /*
         * These variables allow os to reference a camera, a "Line Renderer" component and a "Distance Joint 2D" component
         */

        [SerializeField]    private AudioSource shootSound;
        //This variable addresses the audio source which is required to play the shoot sound.

       
        void Start()
        {
            _distanceJoint.enabled = false;
            // At the start of a scene, we disable the distance joint
        }
        
        
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                // Here we are checking if the left mouse button has been pressed
                
                
                Vector2 mousePos = (Vector2) mainCamera.ScreenToWorldPoint(Input.mousePosition);
                _lineRenderer.SetPosition(0, mousePos);
                _lineRenderer.SetPosition(1, transform.position);
                _distanceJoint.connectedAnchor = mousePos;
                _distanceJoint.enabled = true;
                _lineRenderer.enabled = true;
                /*
                 * If so, we are creating a local variable named "mousePos", which uses a Unity-built-in method called
                 * "ScreenToWorldPoint" to pinpoint the exact coordinates where the mouse button has been clicked and transforms
                 * it into the game. After that we are setting the start- and endpoint of the line renderer to the position
                 * of the player and the mouse position. The distance joint anchors at the location the mouse button has been
                 * pressed, so the grappling hook won't move with the main object it is attached to.
                 * Both, line renderer and distance joint, are being enabled.
                 */

                shootSound.Play();
                //When the key is pressed the audio attached to the source will be played
            }
            else if (Input.GetKeyUp(KeyCode.Mouse0))
            {
                // If the mouse button press has stopped...
                
                
                _distanceJoint.enabled = false;
                _lineRenderer.enabled = false;
                /*
                 * ... disable both, distance joint and line renderer, in that exact moment.
                 */
                
            }

            if (_distanceJoint.enabled)
            {
                // If the distance joint has been enabled,
                
                
                _lineRenderer.SetPosition(1, transform.position);
                /*
                 * ... set the position of the line renderer to the position of the object it is attached to.
                 */
                
            }
        }
    }
}