using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PTWO_PR
{
    public class SpawnerMover : MonoBehaviour
    {
        /*
         * This small script simply makes any object it is attached to move along the "Y" axis. In this case,
         * this script is attached to the asteroid spawner objects in Unity.
         */ 
        
        
        [SerializeField] private float movementSpeed;
        // This variable lets us adjust the movement speed of the objects in the Unity editor

        private void Update()
        {
            Vector3 movement = new Vector3(0f, 1f, 0f);
            transform.position += movement * (Time.deltaTime * movementSpeed);
            /*
             * At first we create a local variable of the type "Vector3" called "movement" and set it equal to a
             * new "Vector3" value. In this case, we set the movement speed on the "X" and "Z" axis to 0 and to 1 on
             * the "Y" axis. This way we make sure that the object will only move on the "Y" axis.
             * After that we access the current position of the object and add the local "movement" variable (with the
             * new "Vector3" instructions) multiplied by "Time.deltaTime" and the value of the "movementSpeed" variable,
             * which we determined in the Unity inspector. "Time.deltaTime" makes sure that our movement speed is always
             * on the same level.
             */
        }
    }
}

