﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PTWO_PR
{
    public class Restart : MonoBehaviour
    {
        // Enables the player to quickly restart the game scene by pressing the "R" key
        
        
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                // Once an input on the "R" key is registered, do the following:
                
                
                SceneManager.LoadScene("MAIN_GAME");
                /*
                 * Access the "SceneManager" class by using the "LoadScene" method and reload the scene referenced in
                 * the parentheses.
                 */ 

            }
        }
    }
}