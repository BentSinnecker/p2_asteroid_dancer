﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace PTWO_PR
{
    public class ExitGame : MonoBehaviour
    {
        /*
         * This script lets the player leave the game scene and return to the main menu by pressing the "ESC" button
         * on the keyboard. It is attached to a game manager object in the game scene
         */
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                // If-function checks whether the "ESC" button is pressed once
                
                SceneManager.LoadScene("MAIN_MENU");
                /*
                 * Here we access the "SceneManager" class with the "LoadScene" method by using the "."-operator.
                 * In the "LoadScene" method we define the scene we want to load, which is the "MAIN_MENU" scene
                 * in this case.
                 */
            }
        }
    }
}