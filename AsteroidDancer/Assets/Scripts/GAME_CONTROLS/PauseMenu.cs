﻿using System;
using System.Runtime.CompilerServices;
using Unity.IO.LowLevel.Unsafe;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PTWO_PR
{
    public class PauseMenu : MonoBehaviour
    {
        public static bool GameIsPaused = false; 
        /*
         * This variable keeps track of whether this game is currently paused. It is "public" because it we want it to be
         * accessible from other scripts and "static" because we don't want to reference this specific pause menu script, we
         * just want to easily check from other scripts whether the game is paused. This is useful if you have a sound
         * manager script and want to lower the volume during pauses or cut it out completely.
         */

        [SerializeField] private GameObject pauseMenuUI;
        // Used to access the game object this script is attached to and to create a reference.

        [SerializeField] private string sceneName;
        // Used to write in the Unity inspector what the scene name is

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                // If-function checks whether the "ESC" button is pressed
                
                if (GameIsPaused)
                {
                    // If the value of the static bool defined above is true, run the "Resume" method
                    Resume();
                }
                else
                {
                    Pause();
                    // If the value of the static bool defined above is false, run the "Pause" method
                }
            }
        }

        public void Resume()
        {
            // This method is public so it can be accessed by the button UI
            
            pauseMenuUI.SetActive(false);
            /*
             * We access the game object through the "pauseMenuUi" variable defined above and the "."-operator to run
             * the Unity-built-in method "SetActive" and set this method to false
             */

            Time.timeScale = 1f;
            // Is used to set the game to a normal speed rate

            GameIsPaused = false;
            // We're updating the current status of the "GameIsPaused" bool to "false"
        }

        private void Pause()
        {
            pauseMenuUI.SetActive(true);
            /*
             * We access the game object through the "pauseMenuUi" variable defined above and the "."-operator to run
             * the Unity-built-in method "SetActive" and set this method to true
             */

            Time.timeScale = 0f;
            // Is used to freeze the time of the game

            GameIsPaused = true;
            // We're updating the current status of the "GameIsPaused" bool to "true"
        }

        public void LoadMenu()
        {
            // This method is public so it can be accessed by the button UI

            Time.timeScale = 1;
            // Setting the time back to normal so the game won't be stopped while we're in the menu
            
            SceneManager.LoadScene(sceneName);
            /*
             * Here we reference the scene the game should load once the "LoadMenu" method is activated. The scene name
             * is stored in a variable that is stated at the beginning of this script
             */

        }

        public void QuitGame()
        {
            // This method is public so it can be accessed by the button UI
            
            Debug.Log("Quitting game");
            /*
             * Because the "Application.Quit()" function doesn't work in the Unity game viewer we're adding a short
             * message in the debug log which shows us that everything works
             */
            Application.Quit();
            // In a final Unity build, this function will stop the application immediately
            
            
        }
    }
}