﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PTWO_PR
{
    public class SaveScore : MonoBehaviour
    {
        // This script is used to save the progress of the player
        
        
        [SerializeField] private TextMeshProUGUI highscoreText;
        [SerializeField] private Transform player;
        /*
         * These variables are both referencing a component that we need to display the highscore in the game.
         * The "TextMeshProUGUI" class variable lets us display the actual text in the game, and the Transform variable
         * lets us pull the information of an objects current position.
         */


        private void Start()
        {
            highscoreText.text = PlayerPrefs.GetFloat("Highscore").ToString("0");
            /*
             * At the start of the scene, the text inside the UI box will be set equal to the value of the last save in
             * the "PlayerPref" class. The "PlayerPref" class can store game data between game sessions. In this case,
             * we are storing the biggest "Y" position value inside this class (more on that below).
             */ 
        }

        private void Update()
        {
            if (player.position.y > PlayerPrefs.GetFloat("Highscore", 0))
            {
                /*
                 * If the current "Y" position of the player object is bigger than the last stored value in the "PlayerPref"
                 * file:
                 */
                
                
                PlayerPrefs.SetFloat("Highscore", player.position.y);
                Debug.Log("Highscore registered at " + PlayerPrefs.GetFloat("Highscore"));
                /*
                 * Here we are storing a float value of the name "Highscore" inside the "PlayerPrefs" class. The float
                 * value contains of the current position of the player object on the "Y" axis.
                 * To make sure the new value is registered, we also implemented a "Debug.Log" method, that leaves
                 * us a message in the Unity console if everything is working as planned.
                 */
            }
            
            
        }
    }
}