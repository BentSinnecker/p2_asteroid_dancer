using UnityEngine;
using UnityEngine.UI;

namespace PTWO_PR
{

    public class SoundManager : MonoBehaviour
    {
        [SerializeField]
        private Slider volumeSlider;
        // We need reference to the slider so we can change its value later on

        private void Start()
        {
            if (PlayerPrefs.HasKey("musicVolume"))
            {
                PlayerPrefs.SetFloat("musicVolume", 1);
                Load();
                //If there isnt a save data from a previous game session the value will be set to 1f/ 100%.
            }
            else
            {
                Load();
                //If there is save data only the load method will be called which will apply the value of earlier sessions
            }
        }

        public void ChangeVolume()
        {
            AudioListener.volume = volumeSlider.value;
            Save();
            /*
             * The volume of the game will equal the value of the currentl slider 
             * and will be saved every time the player changes it
             */
        }

        private void Load()
        {
            volumeSlider.value = PlayerPrefs.GetFloat("musicVolume");
            /* 
             * The volume slider value will be set to the value which has been stored
             * in the 'musicVolume' keyname
             */
        }

        private void Save()
        {
            PlayerPrefs.SetFloat("musicVolume", volumeSlider.value);
            //This stores the volume slider value in the keyname 'musicVolume'.
        }
    }
}
