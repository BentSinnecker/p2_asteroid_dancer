﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace PTWO_PR
{
    public class Score : MonoBehaviour
    {
        [SerializeField] private Transform player;
        // Allows us to reference the transform component of the player in the Unity engine

        [SerializeField] private TextMeshProUGUI scoreText;
        // Allows us to reference the text component where we want to display the score/distance
        
        private void Update()
        {
            scoreText.text = player.position.y.ToString("0");
            /*
             * The property "text" allows us to change the content of the component referenced in "scoreText".
             * By setting the current position of the player equal to the text, we can display the position.
             * The method "ToString()" converts the int value of "player.position.y" to a string, so it can be
             * displayed properly. By writing a "0" in "ToString" we are formatting the method to only display whole
             * numbers.
             */
        }
    }
}