﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace PTWO_PR
{
    public class ExitApp : MonoBehaviour
    {
        // This script enables the player to close the game by pressing the exit button in the main menu
        public void Exit()
        {
            Application.Quit();
            //This is a common Unity function to quit the application that is currently playing
        }
    }
}