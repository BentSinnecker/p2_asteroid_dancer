﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace PTWO_PR
{
    public class SceneLoader : MonoBehaviour
    {
        // Used to load scenes
        
        
        private Button button;
        
        [SerializeField] string sceneToLoad;

        private void Awake()
        {
            button = GetComponent<Button>();
            // On awake, we assign the component "Button" to our "Button" class
        }

        private void OnEnable()
        {
            button.onClick.AddListener(LoadScene);
            // On enable, a listener method awaits an input assigned to the method "LoadScene" below
        }

        private void OnDisable()
        {
            button.onClick.RemoveListener(LoadScene);
            // On disable, we remove the listener
        }

        private void LoadScene()
        {
            SceneManager.LoadScene(sceneToLoad);
            /*
             * Here, the "SceneManager" class is told what scene should be loaded. We add value to the "sceneToLoad"
             * variable in the Unity inspector.
             */ 
        }
        
    }
}