﻿using UnityEngine;
using UnityEngine.UI;

namespace PTWO_PR
{
    public class FuelBar : MonoBehaviour
    {
        // This script enables us to adjust the fuel indicator in our game UI
        
        
        [SerializeField] private Slider slider;
        // By referencing the component "Slider", we can adjust the length and visibility of the slider


        public void SetMaxFuel(float fuel)
        {
            /*
             * We are leaving the access modifiers of both methods on "public", so that we can access them outside
             * of this script.
             */
            
            
            slider.maxValue = fuel;
            slider.value = fuel;
            /*
             * Here we are setting both, the maxValue and the value of the slider component to the parameter. This
             * helps us defining what the max fuel value will be later on. This method gets called in the "Awake" method
             * of our player script.
             */
        }
        
        public void SetFuel(float fuel)
        {
            slider.value = fuel;
            /*
             * Here we are setting the value of the slider to whatever is assigned as the parameter. In this case,
             * we are using the value of of the float "currentFuel" in the player script to accurately depict the current
             * fuel amount.
             */ 
        }
    }
}