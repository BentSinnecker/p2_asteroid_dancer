﻿using System;
using UnityEngine;

namespace PTWO_PR
{
    public class CameraFollow : MonoBehaviour
    {
        [SerializeField] private Transform player; 
        // Allows us to reference the player transform component
        
        private void Update()
        {
            // In this method we tell the camera to follow our player game object
            
            
            if (player.position.y > transform.position.y)
            {
                /*
                 * If the position of the "player" variable we defined above is bigger than the current position
                 * this script is attached to, do:
                 */
                transform.position = new Vector3(transform.position.x, player.position.y, transform.position.z);
                /*
                 * Set our current position on the "Y" axis equal to the position of the player object, so both positions
                 * meet.
                 */
                
            }
        }
    }
}