﻿namespace PTWO_PR
{
    public static class GameTags
    {
        /*
         * This class is a static class. Its purpose is to provide blueprints of its inherent classes.
         * In this case, we are using static variables to define game tags, which can be used by other scripts.
         */
        
        
        public static string PLAYER = "Player";

        public static string DEATHZONE = "Deathzone";

        public static string ASTEROIDSPAWNMANAGER = "AsteroidSpawnManager";

        public static string COLLECTIBLE = "Collectible";
    }
}