using UnityEngine;

namespace PTWO_PR
{
    public class MusicManager : MonoBehaviour
    {
        private static MusicManager musicManagerInstance;
        //This variable keeps a reference to the Music Manager Game Object.

        private void Awake()
        {
            DontDestroyOnLoad(gameObject);

            if(musicManagerInstance == null)
            {
                musicManagerInstance = this;
            }
            else
            {
                Destroy(gameObject);
                return;
            }
            /*
             * This Method assigns a reference to the Music Manager Object if there isnt one yet.
             * So to keep the Music running between Scenes it'll destroy the old reference and instantiate a new one.
             */
        }
    }
}
