using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PTWO_PR
{
    public class HighlightObject : MonoBehaviour
    {
        private SpriteRenderer asteroid;

        [Header("Color Settings:")]
        [SerializeField] private Color colorDefault;
        
        [SerializeField] private Color colorSelected;
        /*
         * These variables can be altered in the Inspector. 
         * They define which color the sprites have by default and to which color they shall 
         * change when selected.
         */

        private void Start()
        {
            asteroid = GetComponent<SpriteRenderer>();
            //In this Start method we need to fetch the sprite renderer of our asteroids for it to change its color.
        }

        private void OnMouseOver()
        {
            asteroid.color = colorSelected;
            /* 
             * This method changes the color of our asteroids to the selected in the unity inspector
             * when the Mouse Cursor hovers over the sprite.
             */
        }

        private void OnMouseExit()
        {
            asteroid.color = colorDefault;
            /*
             * When the mouse cursor is no longer hovering over the spite the sprite color will change 
             * to a selected default color in the unity inspector.
             */
        }
    }
}