﻿using UnityEngine;

namespace PTWO_PR
{
    public class AsteroidManager : MonoBehaviour
    {
        /*
         * This script is used to destroy all asteroid objects that collide with the deathzone. It is attached
         * to all asteroid-prefabs
         */
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            //This method gets called as soon as one object collides with another
            
            SpawnAsteroidsManager manager =
                GameObject.FindWithTag(GameTags.ASTEROIDSPAWNMANAGER).GetComponent<SpawnAsteroidsManager>();
            /*
             * Here we create a local variable named "manager" which is accessing the script "SpawnAsteroidsManager"
             * and use the "FindWithTag" method to determine the object we want to reference in this local variable.
             * In this case we created a specific game tag in the Unity editor and assigned it to the "AsteroidSpawnManager"
             * object, which stores all asteroids via the "SpawnAsteroidsManager" script.
             * By using the "GetComponent" method, we get access the components of the "SpawnAsteroidsManager" script.
             */

            if (other.CompareTag(GameTags.DEATHZONE))
            {
                /*
                 * If the object this script is attached to collides with another object that has the game tag "Deathzone",
                 * do the following:
                 */
                
                manager.ProcessAsteroids(gameObject);
                /*
                 * Here we are referencing the "ProcessAsteroids" method that we defined in the "SpawnAsteroidsManager"
                 * script by using the local variable we defined above. This method says that the game object this script
                 * is attached to shall be destroyed.
                 */
            }
        }
    }   
}