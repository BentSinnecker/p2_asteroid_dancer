﻿using UnityEngine;

namespace PTWO_PR
{
    public class SpawnAsteroidsManager : MonoBehaviour
    {
        // This script is attached to the "AsteroidSpawnManager" object 
        
        [SerializeField] private Transform[] spawnPoints;

        [SerializeField] private GameObject[] asteroidPrefabs;
        /*
         * These two variables above are so called arrays and can store lists of classes and variables.
         * The first array references the transform component of an object, which means we can store the transform
         * information of any object in this array.
         * The second array references game objects as a whole.
         */

        
        [SerializeField]    private float spawnTime;
        
                            private float timer;
        // The two variables above are used to adjust the speed at which the asteroids should be spawned
        
        
        private void Start()
        {
            timer = spawnTime;
            /*
             * At the start of the scene we set the "timer" value equal to the "spawnTime" value. This way the timer
             * can start to run down
             */
        }

        private void Update()
        {
            timer -= Time.deltaTime;
            /*
             * Here we subtract the timer by "Time.deltaTime", which means we have a steady decline of the "timer" value
             * each frame, because we're in the "Update" method right now. "Time.deltaTime" is the completion time in
             * seconds since the last frame. This helps us to make the game frame-independent
             */
            
            if (timer <= 0f)
            {
                // If the value of the "timer" value is smaller or equal to 0:
                timer = spawnTime;
                SpawnAsteroid();
                /*
                 * Reset the timer value to match the "spawnTime" value again and call the method "SpawnAsteroid",
                 * which we defined below
                 */
            }
        }

        
        private void SpawnAsteroid()
        {
            // Here we determine what asteroid spawns at what location (Spoiler, it's random)
            
            int randomFallingObjectIndex = Random.Range(0, asteroidPrefabs.Length);
            int randomSpawnPointIndex = Random.Range(0, spawnPoints.Length);
            /*
             * Above we define two local variables of type "int" that are both set equal to "Random.Range", which just randomly calls whatever
             * we assign to this function inbetween a range of two int-values. In this case, we set both functions to 0 on the smallest
             * int-value and reference the length of both arrays from the beginning of the script as the biggest values.
             * By referencing the length and not a fixed value, we have the chance to add or remove elements to the
             * array in the Unity editor as we please.
             */

            GameObject newFallingObject = Instantiate(asteroidPrefabs[randomFallingObjectIndex]);
            /*
             * Here we define another local variable, this time of the type "GameObject". It's purpose is to spawn, or
             * "Instantiate", a new asteroid randomly from the array "asteroidPrefabs" (the prefabs are assigned to the
             * array in the Unity inspector of the game object this script is attached to). 
             */
            

            newFallingObject.transform.position = spawnPoints[randomSpawnPointIndex].localPosition;
            /*
             * At this point we set the position of the spawned game objects equal to the current position of any
             * spawn point. Because the spawn points are always moving, and have to stay in front of the player in order
             * to make the game work, we have to use the "localPosition" of the spawn points to make sure the the new
             * asteroids will be spawned in front of the the player, not behind.
             */
        }
        
        public void ProcessAsteroids(GameObject asteroidObject)
        {
            
            Destroy(asteroidObject);
            /*
             * Here we use the "Destroy" method to destroy asteroids that have no use anymore. This method gets called
             * in the "AsteroidManager" script.
             */ 
        }
    }
}